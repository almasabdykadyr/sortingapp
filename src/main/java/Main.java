import java.util.Arrays;
import java.util.Random;

/**
* Main app class, fills up an array with numbers, then prints out sorted array
*
* */
public class Main {

    private static final int BOUNDARY = 100;

    public static void main(String[] args) {
        int[] array = new int[10];
        fillArray(array);

        System.out.printf("Initial array: %s \n", Arrays.toString(array));
        Sorting.sort(array);
        System.out.printf("Result array: %s", Arrays.toString(array));
    }

    /**
     * Fills given array with the random numbers with the top boundary
     * equals to 100.
     *
     * @param array an empty array to be filled
     */
    private static void fillArray(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(BOUNDARY);
        }
    }
}
