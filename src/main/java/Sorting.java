/**
 * FIXME: Documentation should be placed above the class or/and method
 * Class for sorting, based on merge sort,
 * also have check for null, and values count
 * FIXME: Author field is redundant. We have GitBlame for it
 *
 * @author Almas Abdykadyr "almasabdykadir@gmail.com"
 **/

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class Sorting {

    private static final Logger logger = LoggerFactory.getLogger(Sorting.class);

    /**
    * Sorts given int array, by using merge sort
    *
    *
    * */
    public static void sort(int[] array) {

        logger.info("Check for null");
        if (array == null) {
            throw new IllegalArgumentException("Array is null");
        }

        logger.info("Check for correct size");

        if (array.length > 10) {

            throw new IllegalArgumentException("You've entered out of 10 numbers");
        } else if (array.length < 10) {

            throw new IllegalArgumentException("You've entered least than 10 numbers");
        }

        logger.info("Start sorting");
        mergeSort(array);
    }

    private static void mergeSort(int[] array) {
        int n = array.length;

        if (n == 1) return;

        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = array[i];
        }

        for (int i = mid; i < n; i++) {
            r[i - mid] = array[i];
        }

        mergeSort(l);
        mergeSort(r);
        merge(array, l, r);
    }

    private static void merge(int[] array, int[] right, int[] left) {

        int leftSize = left.length;
        int rightSize = right.length;
        int i = 0;
        int j = 0;
        int idx = 0;

        while (i < leftSize && j < rightSize) {
            if (left[i] < right[j]) {
                array[idx] = left[i];
                i++;
            } else {
                array[idx] = right[j];
                j++;
            }

            idx++;
        }

        for (int iter = i; iter < leftSize; iter++) {
            array[idx++] = left[iter];
        }

        for (int iter = j; iter < rightSize; iter++) {
            array[idx++] = right[iter];
        }
    }
}
