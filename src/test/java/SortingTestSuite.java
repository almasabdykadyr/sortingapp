import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SortingTest.class,
        TestWhenElementsCountBelowTen.class,
        TestWhenElementsCountGreaterTen.class})
//TODO: add null test
public class SortingTestSuite {

}
