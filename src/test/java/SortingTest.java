import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

//FIXME: "TEST" part of the class name should be at the end
@RunWith(Parameterized.class)
public class SortingTest {

    private final int[] array;
    private final int[] result;

    public SortingTest(int[] array, int[] result) {
        this.array = array;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new int[][][]{
                {{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {{1, 23, 6, 5, 54, 342, 56, 567, 8765, 6555656}, {1, 5, 6, 23, 54, 56, 342, 567, 8765, 6555656}},
                {{0, 1, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 1}}
        });
    }

    @Test
    public void defaultTest() {
        Sorting.sort(array);
        assertArrayEquals(result, array);
    }
}
