import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class TestWhenElementsCountBelowTen {
    private final int[] array;

    public TestWhenElementsCountBelowTen(int[] array) {
        this.array = array;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new int[][]{
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {1, 2, 3, 4, 5, 6, 7, 8},
                {1, 2, 3, 4, 5, 6, 7},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4},
                {1, 2, 3},
                {1, 2},
                {1},
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testElementsCountBelowTen() {
        Sorting.sort(array);
    }

}
