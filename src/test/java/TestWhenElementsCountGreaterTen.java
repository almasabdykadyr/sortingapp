import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;


@RunWith(Parameterized.class)
public class TestWhenElementsCountGreaterTen {

    private final int[] array;

    public TestWhenElementsCountGreaterTen(int[] array) {
        this.array = array;
    }

    @Parameterized.Parameters
    public static List<int[]> testCases() {
        return Arrays.asList(new int[][]{
                {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testElementsCountGreater() {
        Sorting.sort(array);
    }

}